<?php

/**
 * @file
 * Provides node-based cache option for panel panes and displays.
 * Based upon code of simple cache plugin.
 */

/**
 * Specially named implementation of hook_panels_cache()
 */
function ap_cache_node_panels_cache() {
  return array(
    'title' => t("Node based cache"),
    'description' => t('Node based cache stores entries per node id and expires on node update events, with optional time-based expiration.'),
    'cache get' => 'ap_cache_node_cache_get_cache',
    'cache set' => 'ap_cache_node_cache_set_cache',
    'cache clear' => 'ap_cache_node_cache_clear_cache',
    'settings form' => 'ap_cache_node_cache_settings_form',
    'settings form submit' => 'ap_cache_node_cache_settings_form_submit',
    'defaults' => array(
      'context_granularity' => 0,
      'arg_granularity' => 0,
      'role_granularity' => 0,
      'lifetime' => 0,
    ),
  );
}

/**
 * Get cached content.
 */
function ap_cache_node_cache_get_cache($conf, $display, $args, $contexts, $pane = NULL) {
  // Check serials. One missing serial is enough for content cache miss.
  if (!ap_cache_context_serial('check', $contexts, $conf)) {
    //drupal_set_message('Serial and cache miss!');
    return FALSE;
  }
  
  // Check cache.
  $cid = ap_cache_node_cache_get_id($conf, $display, $args, $contexts, $pane);
  $cache = cache_get($cid, 'cache_panels');
  if (!$cache) {
    // Content cache miss.
    //drupal_set_message('Cache miss!');
    return FALSE;
  }
  
  // Enforce lifetime expiration if configured.
  if ($conf['lifetime'] > 0 && (time() - $cache->created) > $conf['lifetime']) {
    //drupal_set_message('Cache timeout!');
    return FALSE;
  }

  return $cache->data;
}

/**
 * Set cached content.
 */
function ap_cache_node_cache_set_cache($conf, $content, $display, $args, $contexts, $pane = NULL) {
  $cid = ap_cache_node_cache_get_id($conf, $display, $args, $contexts, $pane);
  cache_set($cid, $content, 'cache_panels');
}

/**
 * Clear cached content for specified display.
 *
 * Because we aim to memcache setups, which doesn't directly support partial 
 * wildcard flushes, we just clear the whole cache bin.
 * 
 * TODO: It's possible to support partial per-display flush same way as we do 
 * per-node ones: by emulating namespace with serial number. It may be not worth 
 * the hassle though.
 */
function ap_cache_node_cache_clear_cache($display) {
  cache_clear_all('*', 'cache_panels', TRUE);
  
  // Maybe unnecessary, but won't do harm either.
  ap_cache_node_serial('flush');
}

/**
 * Figure out an id for our cache based upon input and settings.
 */
function ap_cache_node_cache_get_id($conf, $display, $args, $contexts, $pane) {
  global $user;
  $id = 'node_cache';

  // This is used in case this is an in-code display, which means did will be something like 'new-1'.
  if (isset($display->owner) && isset($display->owner->id)) {
    $id .= ':' . $display->owner->id;
  }
  $id .= ':' . $display->did;

  if ($pane) {
    $id .= ':' . $pane->pid;
  }
  
  // "Admin links" granularity. Useful if you don't want to enable full role 
  // granularity, but still want to show admin links to users with
  // corresponding permission. 
  if (user_access('view pane admin links')) {
    $id .= ':admin';
  }
  
  // Role granularity
  if ($conf['role_granularity']) {
    $id .= ':roles';
    $roles = $user->roles;
    ksort($roles); // We want exactly same order of roles.
    foreach ($roles as $rid => $role) {
      $id .= ':' . $rid;
    }
  }

  // Context granularity 
  if ($conf['context_granularity']) {
    $id .= ':contexts';
    if (!is_array($contexts)) {
      $contexts = array($contexts);
    }
    foreach ($contexts as $context) {
      if (isset($context->argument)) {
        $id .= ':' . $context->argument;
      }
    }
  }
  
  // Argument granularity
  if ($conf['arg_granularity']) {
    $id .= ':args';
    foreach ($args as $arg) {
      $id .= ':' . $arg;
    }
  }
  
  // Node contexts serial (namespace emulation).
  $id .= ap_cache_context_serial('get', $contexts, $conf);
  
  return $id;
}

/**
 * Perform context serial operations.
 * 
 * @param $op
 *  Operation, one of the following:
 *   "get"   - returns complete serial part of content cache key for 
 *             combination of contexts.
 *   "check" - checks every context serial existance. Returns FALSE on first
 *             missing serial, or TRUE if every serial is found.
 *
 * @param $contexts
 *  Contexts array. Should contain context objects.
 *  
 * @param $conf
 *  Configuration array
 *  
 * @return unknown_type
 *  Depends on operation. 
 */
function ap_cache_context_serial($op, $contexts, $conf) {
  // Prepare contexts array.
  if (!is_array($contexts)) {
    $contexts = array($contexts);
  }
  
  $serials = '';
  foreach ($contexts as $key => $context) {
    // TODO: We rely here on $contexts array keyed by context id's.
    // It may be not always so, and $context->id is not available for every 
    // context either. Need to have more reliable way of identifying
    // contexts.
    if (isset($conf['invalidation'][$key]) && $conf['invalidation'][$key] !== 0) {
      switch ($op) {
        case 'check':
          if (!ap_cache_node_serial('check', $context->argument)) {
            return FALSE;
          }
          break;
          
        case 'get':
          $serials .= ':' . ap_cache_node_serial('get', $context->argument);
      }
    }
  }
  
  switch ($op) {
        case 'check':
          return TRUE;
        
        case 'get':
          return ':serial' . $serials;
  }
}



function ap_cache_node_cache_settings_form($conf, $display, $pid) {
  // TODO: This part needs improvement: we need to operate on different
  // node contexts and relationships. Problem is some contexts/relationships
  // don't seem to have $context->id, and we use contexts array key, which
  // doesn't seem to be reliable either. 
  
  // We operate on node-type contexts only. Og is node context too.
  $supported_contexts = array('node', 'og');  
  $options = array();
  foreach ($display->context as $key => $context) {
    if (in_array($context->type, $supported_contexts)) {
      $options[$key] = $context->identifier;
    }
  }
  
  $form['invalidation'] = array(
    '#title' => t('Invalidation source'),
    '#type' => 'checkboxes',
    '#options' => $options,
    '#description' => t('Select combination of cache invalidation sources. When any node in this list changes, cache expires for any specific combination containing that node.'),
    '#default_value' => $conf['invalidation'],
  ); 

  $form['context_granularity'] = array(
    '#title' => t('Context granularity'),
    '#type' => 'checkbox',
    '#description' => t('if enabled, this content will be cached per unique context in the pane or display. If disabled, cache will be same for any context.'),
    '#default_value' => $conf['context_granularity'],
  );
  
  $form['arg_granularity'] = array(
    '#title' => t('Argument granularity'),
    '#type' => 'checkbox',
    '#description' => t('If enabled, this content will be cached per individual argument to the entire display. If disabled, cache will be same for any arguments.'),
    '#default_value' => $conf['arg_granularity'],
  );

  $form['role_granularity'] = array(
    '#title' => t('Role granularity'),
    '#type' => 'checkbox',
    '#description' => t('If enabled, this content will be cached per combination of user roles. If disabled, cache will be same for all user roles.'),
    '#default_value' => $conf['role_granularity'],
  ); 

  $options = drupal_map_assoc(array(0, 15, 30, 60, 120, 180, 240, 300, 600, 900, 1200, 1800, 3600, 7200, 14400, 28800, 43200, 86400, 172800, 259200, 345600, 604800), 'format_interval');
  $form['lifetime'] = array(
    '#title' => t('Force lifetime'),
    '#type' => 'select',
    '#options' => $options,
    '#description' => t("If set to greater than 0, cache will expire independently from node updates. 0 means lifetime won't be forced."),
    '#default_value' => $conf['lifetime'],
  );
  
  return $form;
}
